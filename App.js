import 'react-native-gesture-handler'
import * as React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { ThemeProvider } from './src/theme/provider'
import { MainNav } from './src/routes/main.routes'
import { AuthNav } from './src/routes/auth.routes'
import { EnrlNav } from './src/routes/enrollment.routes'
import { InitScreen } from './src/screens/init/init.container'

const Stack = createStackNavigator()


class App extends React.PureComponent {
  render(){
    return (
      <ThemeProvider>
        <NavigationContainer>
          <Stack.Navigator headerMode="none" screenOptions={{ gestureEnabled: false }}>
            <Stack.Screen name="Init" component={InitScreen}/>
            <Stack.Screen name="Auth" component={AuthNav} />
            <Stack.Screen name='Enrollment' component={EnrlNav} />
            <Stack.Screen name="Main" component={MainNav} />
          </Stack.Navigator>
        </NavigationContainer>
      </ThemeProvider>
    )
  }
}

export default App
