# Chat app

### TODO:
1. Chat functionality
2. Design

React Native app with Firebase integration

### Includes
1. React
2. React Navigation v5
2. Firebase API
3. Styled components

### For build run:
1. yarn
2. react-native run-ios
3. react-native start