import React, { PureComponent } from "react"
import { auth, storage, db } from './config'

const withFirebase = (PassedComponent) =>
  class withFirebase extends PureComponent {
    constructor(props) {
      super(props)
      this.auth = auth
      this.storage = storage
      this.db = db
    }
    
    render() {
      return (
        <PassedComponent
          auth={this.auth}
          storage={this.storage}
          db={this.db}
          {...this.props}
        />
      )
    }
  }

export default withFirebase