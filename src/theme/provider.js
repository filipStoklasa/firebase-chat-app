
import React, { PureComponent } from 'react'
import { ThemeProvider as TP }from 'styled-components'
import { Appearance, AppearanceProvider } from 'react-native-appearance'
import { darkTheme, lightTheme } from './themes'

export class ThemeProvider extends PureComponent {
  state = {
    theme: Appearance.getColorScheme() === 'dark' ? darkTheme : lightTheme
  }

  componentDidMount(){
    this.subscription = Appearance.addChangeListener(({ colorScheme }) => {
      this.setState(() => ({ theme: colorScheme === 'dark' ? darkTheme : lightTheme }))
    })
  }

  componentWillUnmount() {

    if (this.subscription) {
      this.subscription.remove()
    }
  }
    
  render(){
    const { children } = this.props
    const { theme } = this.state
    return (
      <AppearanceProvider>
        <TP theme={theme}>
          {children}
        </TP>
      </AppearanceProvider>
    )
  }
}