
export const lightTheme = {
  primaryColor:'white',
  secondaryColor:'#413c69',
  terciaryColor:'#eab9c9',
  blur:'dark'
}
  
export const darkTheme = {
  primaryColor:'black',
  secondaryColor:'#eab9c9',
  terciaryColor:'#413c69',
  blur:'light'
}
  