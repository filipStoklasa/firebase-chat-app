import styled from 'styled-components'
import {
  space,
  height,
  width,
  borderRadius,
  minWidth,
  minHeight,
  style,
  top,
  left,
  border,
  display,
  position,
  fontSize,
  fontWeight,
  textAlign,
  letterSpacing,
  color,
  fontFamily,
  zIndex,
  flexWrap,
  flexDirection,
  flex,
  flexBasis,
  alignSelf,
  justifyContent,
  alignItems,
  bottom,
  right,
  borderBottom,
} from 'styled-system'

const borderTopLeftRadius = style({
  prop: 'borderTopLeftRadius',
})
const borderTopRightRadius = style({
  prop: 'borderTopRightRadius',
})
const borderBottomLeftRadius = style({
  prop: 'borderBottomLeftRadius',
})
const borderBottomRightRadius = style({
  prop: 'borderBottomRightRadius',
})
const overflow = style({
  prop: 'overflow',
})
const background = style({
  prop: 'background',
  cssProperty: 'background',
  key: 'colors',
})

export const View = styled.SafeAreaView`
  background-color:${({ theme, background }) => background || theme.primaryColor};
  ${position}
  ${zIndex}
  ${display}
  ${space}
  ${top}
  ${left}
  ${bottom}
  ${right}
  ${height}
  ${minHeight}
  ${minWidth}
  ${width}
  ${background}
  ${borderRadius}
  ${borderTopLeftRadius}
  ${borderTopRightRadius}
  ${borderBottomLeftRadius}
  ${borderBottomRightRadius}
  ${border}
  ${overflow}
  ${flexWrap}
  ${flexDirection}
  ${flex}
  ${flexBasis}
  ${alignSelf}
  ${alignItems}
  ${justifyContent}
  ${borderBottom}
`

export const Image = styled.Image`
  ${position}
  ${zIndex}
  ${display}
  ${space}
  ${top}
  ${left}
  ${bottom}
  ${right}
  ${height}
  ${minHeight}
  ${minWidth}
  ${width}
  ${background}
  ${borderRadius}
  ${borderTopLeftRadius}
  ${borderTopRightRadius}
  ${borderBottomLeftRadius}
  ${borderBottomRightRadius}
  ${border}
  ${overflow}
  ${flexWrap}
  ${flexDirection}
  ${flex}
  ${flexBasis}
  ${alignSelf}
  ${alignItems}
  ${justifyContent}
  ${borderBottom}
`

export const Text = styled.Text`
  color: ${({ theme }) => theme.secondaryColor};
  ${space}
  ${position}
  ${fontSize}
  ${fontWeight}
  ${textAlign}
  ${letterSpacing}
  ${color}
  ${top}
  ${left}
  ${bottom}
  ${right}
`

export const  Button = styled.TouchableOpacity`
  background-color: ${({ theme, toggled }) => toggled ? theme.terciaryColor : theme.secondaryColor};
  border-radius: 8px;
  ${position}
  ${zIndex}
  ${display}
  ${space}
  ${top}
  ${left}
  ${bottom}
  ${right}
  ${height}
  ${minHeight}
  ${minWidth}
  ${width}
  ${background}
  ${borderRadius}
  ${borderTopLeftRadius}
  ${borderTopRightRadius}
  ${borderBottomLeftRadius}
  ${borderBottomRightRadius}
  ${border}
  ${overflow}
  ${flexWrap}
  ${flexDirection}
  ${flex}
  ${flexBasis}
  ${alignSelf}
  ${alignItems}
  ${justifyContent}
  ${borderBottom}
`

export const Label = styled(Text)`
  color: ${({ theme }) => theme.primaryColor};
`

export const AuthInput = styled.TextInput`
  borderColor: gray;
  borderBottomWidth: 1px;
  border-color:${({ theme })=>theme.secondaryColor};
  height: 30px;
  color:${({ theme })=>theme.secondaryColor};
  ${space}
  ${position}
  ${fontFamily}
  ${fontSize}
  ${fontWeight}
  ${textAlign}
  ${letterSpacing}
  ${color}
  ${top}
  ${left}
  ${bottom}
  ${right}
`