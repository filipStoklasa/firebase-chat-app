import React, { PureComponent } from 'react'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import { AuthInput, Text, View, Button } from './index'
import { withTheme } from 'styled-components'
import { validationFactory } from '../../utils/validation'

export class ValidateInputView extends PureComponent {
  constructor(props){
    super(props)
    this.state = {
      value:'',
      uncovered: this.props.secure,
      validation: this.props.validate.map((item)=> validationFactory[item]().valid),
      error:false
    }
  }

  onChange = (value) => {
    const { onChange, validate } = this.props
    const valid = validate.map((item)=> validationFactory[item](value).valid)
    this.setState(() => ({ value, validation: valid, error: false }))
    onChange(value, valid.indexOf(false) == -1)
  }

  checkError = () => {
    const { validation } = this.state
    const { validate } = this.props

    if (validation.indexOf(false)!==-1) {
      this.setState(() => ({ error: validationFactory[validate[validation.indexOf(false)]]().message }))
    }
  }

  uncoverPwd = () => this.setState(() => ({ uncovered:!this.state.uncovered }))

  render() {
    const { label, secure, theme, ...rest } = this.props
    const { value, uncovered, error } = this.state
    return (
      <View my={15}>
        <View justifyContent='center'>
          <Text left={10} top={value.length !== 0 && -20 } position='absolute'>{label}</Text>
          <AuthInput onBlur={this.checkError} value={value} onChangeText={this.onChange} secureTextEntry={uncovered} {...rest} />
          {secure && <Button onPress={this.uncoverPwd} background='transparent' p={1} position='absolute' right={0}><FontAwesome name='eye-slash' size={20} color={theme.secondaryColor} /></Button> }
        </View>
        {error && <View background='red' ><Text mx={10} py={10}>{error}</Text></View>}
      </View>
    )
  }
}
export const ValidateInput = withTheme(ValidateInputView)