import React, { PureComponent } from 'react'
import withFirebase from '../../firebase'
import { Button, Image } from '../../components/theme-components'
class Header extends PureComponent {
  navigateSettings =  () => this.props.navigation.navigate('Settings')

  render() {
    const { auth: { currentUser } } = this.props
    return (
      <Button alignItems='center' justifyContent='center' ml={20} borderRadius={15} width={30} height={30} overflow='hidden' onPress={this.navigateSettings}>
          <Image 
            position='absolute' 
            width={30} 
            height={30} 
            source={{ uri: currentUser && currentUser.photoURL ? currentUser.photoURL : 'avatar' }}
          />          
      </Button>
    )
  }
}

export const MainHeader = withFirebase(Header)