import React, { PureComponent } from 'react'
import { Text } from '../theme-components'

export class ModalHeader extends PureComponent {
  navigate =  () => {
    const { navigation: { goBack, navigate }, nextScreen } = this.props

    if (nextScreen) {
      return navigate(nextScreen)
    } else {
      return goBack()
    }

  }

  render() {
    const { text } = this.props
    return <Text onPress={this.navigate} mr={20}>{text}</Text>
  }
}

