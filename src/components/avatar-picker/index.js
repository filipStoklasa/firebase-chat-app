import React, { PureComponent } from "react"
import ImagePicker from 'react-native-image-picker'
import { Button, Image } from '../theme-components'

const options = {
  title: 'Select Avatar',
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
}

export class AvatarPicker extends PureComponent {
  openImagePicker = () => {
    const { onPick } = this.props

    ImagePicker.showImagePicker(options, (response) => {          
      if (response.didCancel) {
        return
      } else if (response.error) {
        return
      } else {
        return onPick(response.uri)
      }
    })

  }

  render() {
    const { avatarSource, ...rest } = this.props
    return (
      <Button alignSelf='center' onPress={this.openImagePicker} alignItems='center' justifyContent='center' width={210} height={210} borderRadius={105} {...rest}>
        <Image width={200} height={200} borderRadius={100} source={{ uri: avatarSource || 'avatar' }}/>
      </Button>
    )
  }
}



