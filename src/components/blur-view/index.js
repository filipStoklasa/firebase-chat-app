import React from 'react'
import { ActivityIndicator }from 'react-native'
import { BlurView } from "@react-native-community/blur"
import { withTheme } from 'styled-components'

export const BlurViewModal = withTheme(({ theme }) => (
  <BlurView
    blurType={theme.blur}
    blurAmount={10}
    style={{
      position: "absolute",
      alignItems:'center',
      justifyContent:'center',
      top: 0,
      left: 0,
      bottom: 0,
      right: 0,
    }}  
  >
    <ActivityIndicator size="large" color="white" />
  </BlurView>
))