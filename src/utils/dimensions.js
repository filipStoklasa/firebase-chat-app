import { Dimensions } from 'react-native'
import { memoize }from './memoize'

const { width, height } = Dimensions.get('window')

export const W = memoize((per) => width * (per / 100))
export const H = memoize((per) => height * (per / 100))