import { memoize } from './memoize'

const required = memoize((val = '') => ({ valid: val.length > 0, message:'Field is required' }))

const isEmail = memoize((val = '') => {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return { valid: re.test(String(val)), message: 'Bad format' }
})

const isPwd = memoize((val = '') => {
  const re = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/
  return { valid: re.test(String(val)), message: "8 characters, one capital letter" }
})

export const validationFactory = {
  isPwd,
  isEmail,
  required
}