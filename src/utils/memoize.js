import { identity, memoizeWith } from 'ramda'

export const memoize = (fn, resolver = identity) => memoizeWith(resolver, fn)
