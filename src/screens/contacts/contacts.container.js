import React, { PureComponent } from 'react'

import { TextInput } from 'react-native'
import { Button, Label, View, Text }from '../../components/theme-components'
import withFirebase from '../../firebase'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'
class ContactsView extends PureComponent {
  state = {
    search:'',
    result:null
  }

  onChange = (value) => {
    this.setState(()=>({ search:value }))
  }
  
  onSearch =async() => {
    const { db } = this.props

    const data = await db.ref('users').once('value').then(function(snapshot) {
      return (snapshot.val() && snapshot.val()) || 'Anonymous'
    })

    Object.keys(data).map((item)=>data[item]['id'] = item)
    this.setState(()=>({ result:Object.values(data) }))
  }

  goToProfile = (id) => () => {
    
    this.props.navigation.navigate('Profile', { id })}

  render() {
    const { search, result } = this.state
    return (
      <View flex={1} >
        <View justifyContent='center' alignItems='center'>
        <Text>Contacts screen</Text>
        <TextInput value={search} onChangeText={this.onChange}/>
        <Button onPress={this.onSearch}><Label>Search</Label></Button>
        </View>
        <ScrollView style={{ flex:1 }}>
          {result && result.map((item)=>(
            <TouchableOpacity key={item.id} onPress={this.goToProfile(item.id)}>
            <View  flexDirection='row'  borderBottomWidth={1}>
              <Text px={10} py={10} color='red'>{item.displayName || item.email}</Text>
            </View>
            </TouchableOpacity>
          ))}
        </ScrollView>
      </View>
    )
  }

}
export const ContactsScreen = withFirebase(ContactsView)