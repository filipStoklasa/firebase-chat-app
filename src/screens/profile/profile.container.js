import React, { PureComponent } from 'react'

import { View, Text, Button, Label } from '../../components/theme-components'
import withFirebase from '../../firebase'
import { ActivityIndicator } from 'react-native-paper'

class ProfileView extends PureComponent {
  state = {
    profile:null
  }

  async componentDidMount(){
    const { route:{ params }, db } = this.props
    const data = await db.ref(`users/${params.id}`).once('value').then(function(snapshot) {
      return (snapshot.val() && snapshot.val()) || 'Anonymous'
    }) 

    this.setState(()=>({ profile:data }))
  }

  sendFriendRequest = async () => {
    const { route:{ params }, db, auth } = this.props
    await db.ref('friendships/' + params.id + '/requested/').update({
      [auth.currentUser.uid]: true,
    })
    await db.ref('friendships/' + auth.currentUser.uid + '/pending/').update({
      [auth.currentUser.uid]: true,
    })
  }

  render() {
    const { profile } = this.state 
    if(!profile){
      return <ActivityIndicator style={{ flex:1 }}  size='large'/>
    }
    return (
      <View flex={1} alignItems='center' justifyContent='center'>
        <Text>Profile Screen</Text>
        {profile.email && <Text>{profile.email}</Text>}
        <Button onPress={this.sendFriendRequest}>
        <Label>Add friend</Label>
        </Button>
      </View>
    )
  }
}
export const ProfileContainer = withFirebase(ProfileView)