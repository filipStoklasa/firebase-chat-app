import React, { PureComponent } from 'react'

import { Text, View, Button, Label } from '../../components/theme-components'
import withFirebase from '../../firebase'

class SettingScreen extends PureComponent{
  signOut = () => this.props.auth.signOut()
  
  render() {
    return (
      <View flex={1} alignItems='center' justifyContent= 'center' >
        <Text>Settings Screen</Text>
        <Button onPress={this.signOut} mt={30} px={20} py={10}>
          <Label>Sign out</Label>
        </Button>
      </View>
    )
  }

}

export const SettingContainer = withFirebase(SettingScreen)