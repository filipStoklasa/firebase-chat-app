import React, { PureComponent } from 'react'

import { View, Text } from '../../components/theme-components'

export class HomeScreen extends PureComponent {
  render() {
    return (
      <View flex={1} alignItems='center' justifyContent='center'>
        <Text>Home Screen</Text>
      </View>
    )
  }

}
