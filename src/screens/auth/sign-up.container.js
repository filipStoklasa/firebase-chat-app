import React, { PureComponent } from 'react';
import { View, Text, Button, Label, Image }from '../../components/theme-components'
import { ValidateInput } from '../../components/theme-components/auth-input'
import { KeyboardAvoidingView }from 'react-native'
import { W } from '../../utils/dimensions'
import withFirebase from '../../firebase'
import { StackActions }from '@react-navigation/native'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import { withTheme } from 'styled-components'

class SignUpScreen extends PureComponent {
  constructor() {
    super()
    this.state = {
      email: '',
      password: '',
      confirmed: false,
      valid: {
        email: false,
        password: false,
        confirmed: false
      }
    }
  }

  onChangeEmail = (email, validate) => {
    this.setState(() => ({ email, error: null, valid: { ...this.state.valid, email: validate } }))
  }

  onChangePassword = (password, validate) => {
    this.setState(() => ({ password, error: null, valid: { ...this.state.valid, password: validate } }))
  }

  onConfirm = () => {
    const { confirmed } = this.state
    this.setState(() => ({ confirmed: !confirmed, valid: { ...this.state.valid, confirmed: !confirmed  } }))
  }

  onSubmit = async() => {
    const { email, password } = this.state
    const { navigation: { dispatch, navigate, pop }, auth, db } = this.props
    navigate('Blur')

    try { 
      await auth.createUserWithEmailAndPassword(email, password)
      await db.ref('users/' + auth.currentUser.uid).set({
        email: email,
        profile_picture : ''
      })
   
      dispatch(StackActions.push('Enrollment'))
    } catch(error) {
      pop()
      this.setState(() => ({ error: error.toString() }))
    }
  }

  render() {
    const { valid, error, confirmed } = this.state
    const { theme } = this.props
    const isValid = Object.values(valid).indexOf(false) == -1

    return (
      <View flex={1} alignItems='center' justifyContent='flex-start'>
        <Image width={W(50)} height={W(50)} borderRadius={W(50)/2} my={40} source={{ uri:'logo' }}/>
        <KeyboardAvoidingView behavior='position'>
          <View width={W(80)} justifyContent='center'>
            {error && <View my={20} borderColor='red' background='pink' ><Text color='red' p={10}>{error}</Text></View>}
            <ValidateInput validate={[ 'required', 'isEmail' ]} label='E-mail' px={10} onChange={this.onChangeEmail}/>
            <ValidateInput validate={[ 'required', 'isPwd' ]} label='Password' px={10} onChange={this.onChangePassword} secure={true} />
            <View mt={30} height={20} flexDirection='row' justifyContent='flex-start' alignItems='center'>
              <Button justifyContent='center' alignItems='center' toggled={confirmed} onPress={this.onConfirm} mr={10} width={20} height={20}>
                <FontAwesome name='check' size={15} color={confirmed ? theme.primaryColor : 'grey'} />
              </Button>
              <Text>Confirm some shit</Text>
            </View>
            <Button disabled={!isValid} background={!isValid && 'grey'} alignItems='center' onPress={this.onSubmit} px={20} py={10} mt={30}>
              <Label>Sign Up</Label>
            </Button>
          </View>
        </KeyboardAvoidingView>
      </View>
    )
  }
}

export const SignUpContainer = withFirebase(withTheme(SignUpScreen))