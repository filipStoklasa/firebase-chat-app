import React, { PureComponent } from 'react'
import { View, Text, Label, Button } from '../../components/theme-components'
import { ValidateInput } from '../../components/theme-components/auth-input'
import withFirebase from '../../firebase'
import { KeyboardAvoidingView } from 'react-native'
import { StackActions }from '@react-navigation/native'
import { AvatarPicker } from '../../components/avatar-picker'
import { W } from '../../utils/dimensions'

class EnrollmentScreen extends PureComponent {
  constructor() {
    super()
    this.state = {
      username: '',
      avatarSource: null,
      valid: {
        username: false,
      }
    }
  }

  onChangeUsername = (username, validate) => {
    this.setState(() => ({ username, valid: { ...this.state.valid, username: validate } }))
  }

  onChangeAvatar = (avatarSource) => {
    this.setState(() => ({ avatarSource }))
  }

  enroll = async() => {
    const { username, avatarSource  } = this.state
    const { navigation: { dispatch, navigate, pop }, auth:{ currentUser }, storage, db } = this.props
    navigate('Blur')
    
    try { 
      let imageAdress = avatarSource
      
      if (imageAdress) {
        const storageRef = storage.ref(`avatars/${currentUser.uid}`)
        const response = await fetch(avatarSource)
        const blob = await response.blob()
        await storageRef.put(blob) 
        imageAdress = await storage.ref().child(`avatars/${currentUser.uid}`).getDownloadURL()
      }
      
      await currentUser.updateProfile({ displayName: username, photoURL:imageAdress || '' })
      await db.ref('users/' + currentUser.uid).update({
        displayName: username,
        profile_picture : imageAdress || ''
      })
      dispatch(StackActions.push('Main'))
    } catch(error) {
      pop()
      return
    }
  }

  render() {
    const { valid, avatarSource } = this.state  
    const isValid = Object.values(valid).indexOf(false) == -1
    return (
      <View flex={1} alignItems='center' justifyContent='flex-start'>
          <AvatarPicker avatarSource={avatarSource} onPick={this.onChangeAvatar} mt={60} /> 
          <Text my={H(20)} textAlign='center'>Select your profile picture</Text>
          <KeyboardAvoidingView behavior='position'>
            <View width={W(80)} justifyContent='center'>
              <ValidateInput validate={[ 'required' ]} label='User name' width={W(80)} px={10} onChange={this.onChangeUsername} />
              <Button diabled={!isValid} background={!isValid && 'grey'} alignItems='center' onPress={this.enroll} px={20} py={10} mt={30}>
                <Label>Done</Label>
              </Button>
            </View>
          </KeyboardAvoidingView>
      </View>
    )
  }
}
export const EnrollmentContainer = withFirebase(EnrollmentScreen)