import React, { PureComponent } from 'react'
import { KeyboardAvoidingView } from 'react-native'
import { View, Button, Label, Image, Text } from '../../components/theme-components'
import { ValidateInput } from '../../components/theme-components/auth-input'
import { W } from '../../utils/dimensions'
import withFirebase from '../../firebase'
import { StackActions } from '@react-navigation/native'

export class SignInScreen extends PureComponent {
  constructor() {
    super()
    this.state = {
      email: '',
      password: '',
      error: null,
      valid: {
        email: false,
        password: false
      }
    }
  }

  onChangeEmail = (email, validate) => {
    this.setState(() => ({ email, error: null, valid: { ...this.state.valid, email: validate } }))
  }

  onChangePassword = (password, validate) => {
    this.setState(() => ({ password, error: null,  valid: { ...this.state.valid, password: validate } }))
  }

  onSubmit = async() => {
    const { email, password } = this.state
    const { navigation:{ navigate, dispatch, pop }, auth } = this.props
    navigate('Blur')

    try {   
      await auth.signInWithEmailAndPassword(email, password)
      dispatch(StackActions.push('Main'))
    } catch(error) {
      pop()
      this.setState(() => ({ error: error.toString() }))
      return
    }
  }

  render() {
    const { valid, error } = this.state
    const isValid = Object.values(valid).indexOf(false) == -1
    
    return (
      <View flex={1} alignItems='center' justifyContent='flex-start'>
        <Image width={W(50)} height={W(50)} borderRadius={W(50)/2} my={40} source={{ uri:'logo' }}/>
        <KeyboardAvoidingView behavior='position'>
          <View width={W(80)} alignSelf='center'>
            {error && <View my={20} borderColor='red' background='pink' ><Text color='red' p={10}>{error}</Text></View>}
            <ValidateInput validate={[ 'required', 'isEmail' ]} label='E-mail' px={10} onChange={this.onChangeEmail}/>
            <ValidateInput validate={[ 'required', 'isPwd' ]} label='Password' px={10} onChange={this.onChangePassword} secure={true} />
            <Button disabled={!isValid} background={!isValid && 'grey'} alignItems='center' onPress={this.onSubmit} px={20} py={10} mt={10}>
              <Label>Sign In</Label>
            </Button>
          </View>
        </KeyboardAvoidingView>
      </View>
    )
  }
}

export const SignInContainer = withFirebase(SignInScreen)