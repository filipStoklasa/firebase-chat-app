import React, { PureComponent } from 'react';
import { auth } from '../../config'
import { ActivityIndicator } from 'react-native'
import { View }from '../../components/theme-components'
import { StackActions } from '@react-navigation/native'

export class InitScreen extends PureComponent {
  state = {
    loading: true
  }

  componentDidMount() {
    const { navigation: { dispatch } } = this.props

    this.init = auth.onAuthStateChanged(user => {
      if (user) {
        dispatch(StackActions.replace('Main'))
      } else {
        dispatch(StackActions.replace('Auth'))
      }
    })

  }

  componentWillUnmount() {
    this.init()
  }

  render() {
    return (
      <View flex={1} alignItems='center' justifyContent='center'>
         <ActivityIndicator size="large" color="grey" />
      </View>
    )
  }
}
