import React, { PureComponent } from 'react'
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs'

const Tab = createMaterialBottomTabNavigator()

export class ThemeTab extends PureComponent{
  render() {
    const { children, theme } = this.props
    return (
      <Tab.Navigator 
        barStyle={{
          backgroundColor: theme.primaryColor
        }}
        activeColor={theme.secondaryColor}
        inactiveColor='grey'
      >
        {children}
      </Tab.Navigator>
    )
  }
}
