import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { BlurViewModal } from '../components/blur-view'

const Stack = createStackNavigator()

export const StackWithModal = ({ children, route }) => (
  <Stack.Navigator 
    headerMode={(route && route.name=="Enrollment") && !(route && route.state && route.state.routes && route.state.routeNames[route.state.index] == 'Blur') ? 'float' : 'none'}
    mode="modal"
    transparentCard={true}
    screenOptions={{ 
      cardStyle: { backgroundColor: 'transparent' },
      gestureEnabled: false,
      cardStyleInterpolator: ({ current: { progress } }) => ({
        cardStyle: {
            opacity: progress.interpolate({
            inputRange: [ 0, 0.5, 0.9, 1 ],
            outputRange: [ 0, 0.25, 0.7, 1 ],
          }),
        },
        overlayStyle: {
          opacity: progress.interpolate({
            inputRange: [ 0, 1 ],
            outputRange: [ 0, 0.5 ],
            extrapolate: 'clamp',
          }),
        },
      }),
    }}
    getstureEnabled={false}
  >
    {children}
    <Stack.Screen 
      options={{ 
        headerLeft: () => null, 
        headerTitle: () => null, 
        headerRight: () => null
      }}   
      name='Blur' 
      component={BlurViewModal}
    />
  </Stack.Navigator>
)
