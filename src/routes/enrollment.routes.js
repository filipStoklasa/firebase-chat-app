import React from 'react'
import { EnrollmentContainer } from '../screens/auth/enrollment.container'

import { createStackNavigator } from '@react-navigation/stack'
import { StackWithModal } from './theme-modal'
import { ModalHeader } from '../components/header/headers'
import { withTheme } from 'styled-components'

const Stack = createStackNavigator()

export const EnrlNav = withTheme(({ navigation, route, theme }) => {
  return (
  <StackWithModal route={route}>
    <Stack.Screen 
      name='Enrollment'  
      component={EnrollmentContainer}
      options={{
        headerStyle: {
          backgroundColor: theme.primaryColor,
          shadowRadius: 0,
          shadowOffset: {
              height: 0,
          },
        },
        headerLeft: () => null, 
        headerTitle: () => null, 
        headerRight: () => <ModalHeader navigation={navigation} textAlign='right' nextScreen='Main' text='Skip'/>
      }}  
    />
  </StackWithModal>
)})