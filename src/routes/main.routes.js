import React, { PureComponent } from 'react'
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { Text } from '../components/theme-components'
import { HomeScreen }from '../screens/home/home.container'
import { ContactsScreen }from '../screens/contacts/contacts.container'
import { createStackNavigator } from '@react-navigation/stack'
import { MainHeader, ModalHeader } from '../components/header/headers'
import { SettingContainer } from '../screens/settings/settings.container'
import { ProfileContainer } from '../screens/profile/profile.container'
import { StackActions }from '@react-navigation/native'
import withFirebase from '../firebase'
import { ThemeTab } from './theme-tab.router'
import { withTheme } from 'styled-components'
import FontAwesome from 'react-native-vector-icons/FontAwesome'

const Stack = createStackNavigator();
const Tab = createMaterialBottomTabNavigator()

const MainTabNavigation = withTheme(({ theme }) => (
  <ThemeTab theme={theme}>
    <Tab.Screen 
      name="Main" 
      options={{
        tabBarLabel: 'Messages',
        tabBarIcon: ({ focused }) => (
          <FontAwesome name="comment" color={focused ? theme.secondaryColor : 'grey'} size={20} />
        ),
      }} 
      component={HomeScreen} 
    />
    <Tab.Screen 
      name="Contacts" 
      options={{
        tabBarLabel: 'Contacts',
        tabBarIcon: ({ focused }) => (
          <FontAwesome name="address-book" color={focused ? theme.secondaryColor : 'grey'} size={20} />
        ),
      }} 
      component={ContactsScreen} 
    />
  </ThemeTab>
))

const MainStackNavigation = withTheme(({ navigation, theme }) => (
  <Stack.Navigator >
    <Stack.Screen 
     name='Main' 
     options={{  
      headerStyle: {
        backgroundColor: theme.primaryColor,
        shadowRadius: 0,
        shadowOffset: {
          height: 0,
      },
    },
      headerLeft: () => <MainHeader navigation={navigation}/>, 
      headerTitle: () => <Text>Chats</Text> 
    }} 
     component={MainTabNavigation} />
  </Stack.Navigator>
))

const SettingNav = withTheme(({ navigation, theme }) => (
  <Stack.Navigator>
    <Stack.Screen 
      name='Settings' 
      options={{ 
        headerStyle: {
          backgroundColor: theme.primaryColor,
          shadowRadius: 0,
          shadowOffset: {
              height: 0,
          },
        },
        headerLeft: () => null, 
        headerTitle: () => null, 
        headerRight: () => <ModalHeader navigation={navigation} text='Done'/> 
      }}  
      component={SettingContainer} 
    />
  </Stack.Navigator>
))

class MainNavRouter extends PureComponent{
  componentDidMount() {
    const { navigation: { dispatch }, auth } = this.props
    auth.onAuthStateChanged(user => {

      if (!user) {
        dispatch(StackActions.replace('Auth'))
      }
      
    })
  }

  render() {
    const { navigation } = this.props
    return (
      <Stack.Navigator 
        mode="modal"
        transparentCard={true}
        headerMode='none'
        waitForRender={true}
      >
        <Stack.Screen 
          name='Main'
          navigation={navigation}
          component={MainStackNavigation}
        />
        <Stack.Screen 
          name='Profile' 
          component={ProfileContainer}
        />
        <Stack.Screen 
          name='Settings'
          navigation={navigation} 
          component={SettingNav}
        />
      </Stack.Navigator>
    )
  }
}

export const MainNav = withFirebase(MainNavRouter)

