import React from 'react'
import { SignInContainer } from '../screens/auth/sign-in.container'
import { SignUpContainer } from '../screens/auth/sign-up.container'
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs'
import { ThemeTab } from './theme-tab.router'
import { createStackNavigator } from '@react-navigation/stack'
import { StackWithModal } from './theme-modal'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import { withTheme } from 'styled-components'

const Tab = createMaterialBottomTabNavigator()
const Stack = createStackNavigator();

const AuthNavTabs = withTheme(({ theme }) => (
  <ThemeTab theme={theme}>
    <Tab.Screen 
      name="SignIn" 
      component={SignInContainer} 
      options={{
        tabBarLabel: 'Sign In',
        tabBarIcon: ({ focused }) => (
          <FontAwesome name="sign-in" color={focused ? theme.secondaryColor : 'grey'} size={20} />
        ),
      }} 
    />
    <Tab.Screen 
      name="SignUp" 
      options={{
        tabBarLabel: 'Sign Up',
        tabBarIcon: ({ focused }) => (
          <FontAwesome name="user-plus" color={focused ? theme.secondaryColor :'grey'} size={20} />
        ),
      }} 
      component={SignUpContainer} />
  </ThemeTab>
))

export const AuthNav = () => (
  <StackWithModal>
    <Stack.Screen name='Auth' component={AuthNavTabs}/>
  </StackWithModal>
)