import Firebase from 'firebase'
// add your custom config https://firebase.google.com/docs/web/setup#config-object
import { config } from './api_config'
let app = Firebase.initializeApp(config)
export const auth = app.auth()
export const storage = app.storage()
export const db = app.database()